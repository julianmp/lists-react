interface ItemActionsProps {
  onMoveDown?: () => void;
  onMoveUp?: () => void;
  onMoveLeft?: () => void;
  onMoveRight?: () => void;
}

export const ItemActions = ({
  onMoveDown,
  onMoveUp,
  onMoveLeft,
  onMoveRight
}: ItemActionsProps) => {
  return (
    <div className="item_actions">
      {onMoveLeft && (
        <button className="buttonLeft" onClick={onMoveLeft}>
          ←
        </button>
      )}
      {onMoveUp && (
        <button className="buttonUp" onClick={onMoveUp}>
          ↑
        </button>
      )}
      {onMoveDown && (
        <button className="buttonDown" onClick={onMoveDown}>
          ↓
        </button>
      )}
      {onMoveRight && (
        <button className="buttonRight" onClick={onMoveRight}>
          →
        </button>
      )}
    </div>
  );
};
