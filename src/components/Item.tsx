import type { FC } from "react";

import { ItemActions } from "./ItemActions";
import { TItem } from "../types";

interface ItemProps {
  info: TItem;
  onMoveUp: () => void;
  onMoveDown: () => void;
  onMoveLeft?: () => void;
  onMoveRight?: () => void;
}

export const Item: FC<ItemProps> = ({
  info,
  onMoveUp,
  onMoveDown,
  onMoveLeft,
  onMoveRight
}) => {
  return (
    <div className="list_item">
      <div>{info.title}</div>
      <ItemActions
        onMoveDown={onMoveDown}
        onMoveUp={onMoveUp}
        onMoveLeft={onMoveLeft}
        onMoveRight={onMoveRight}
      />
    </div>
  );
};
