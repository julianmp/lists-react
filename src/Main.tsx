import { List } from "./components/List";
import { useListGenerator } from "./hooks/useListGenerator";
import { generateInitialItems, InitialItemsConfig } from "./utils";
import minifaker, { uuid } from "minifaker";
import { ListIds } from "./types";

const listItemsConfig: InitialItemsConfig = {
  n: 4,
  idGenerator: uuid.v4,
  parentId: ListIds.list1,
  titleGenerator: () => `${minifaker.firstName()} ${minifaker.lastName()}`
};

// La consigna dice 'Add four more lists' por lo que creo 5 listas.
const initialItems = Array.from({ length: 5 }).map((_, i) =>
  // Genero los items utilizando la misma configuracion por simplicidad.
  generateInitialItems(listItemsConfig)
);

export const Main = () => {
  // Encapsulo la logica de creacion de las listas en un custom hook.
  const {
    lists, // Estado general. Lista de listas
    handleMoveItemLeft, // Mueve un elemento a la lista de la izquierda
    handleMoveItemRight, // Mueve un elemento a la lista de la derecha
    handleMoveItemBuilder // Dado un listIndex crea una funcion para mover elementos internamente
  } = useListGenerator(initialItems);

  return (
    <div className="main">
      {lists.map((list, listIndex) => {
        // Creo la funcion para mover un elemento internamente.
        const handleMoveItem = handleMoveItemBuilder(listIndex);

        return (
          <>
            <List
              key={`list_${listIndex}`}
              items={list}
              onMoveItem={handleMoveItem}
              onMoveItemLeft={(elementIndex) =>
                handleMoveItemLeft(listIndex, elementIndex)
              }
              onMoveItemRight={(elementIndex) =>
                handleMoveItemRight(listIndex, elementIndex)
              }
            />
          </>
        );
      })}
    </div>
  );
};
