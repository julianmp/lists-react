import { ListIds, TItem } from "../types";

const DEFAULT_AMOUNT = 10;

export interface InitialItemsConfig {
  n: number;
  idGenerator: () => string;
  parentId: keyof typeof ListIds;
  titleGenerator?: () => string;
}

export const generateInitialItems = (config: InitialItemsConfig) => {
  return Array.from({ length: config.n ?? DEFAULT_AMOUNT }).map((_, i) => ({
    id: config.idGenerator(),
    parentId: config.parentId,
    title: Boolean(config.titleGenerator)
      ? config.titleGenerator!()
      : "Item - " + i
  })) as TItem[];
};
