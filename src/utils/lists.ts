//--------- Mover elementos en una lista ----------

interface moveItemInListProps {
  list: any[];
  targetIndex: number;
  fromIndex: number;
}

export const moveItemInList = ({
  list,
  targetIndex,
  fromIndex
}: moveItemInListProps) => {
  if (targetIndex < 0) return list;
  const previousListOrder = list.filter((_, i) => i !== fromIndex);
  const itemToAdd = list[fromIndex];

  return [
    ...previousListOrder.slice(0, targetIndex),
    itemToAdd,
    ...previousListOrder.slice(targetIndex)
  ];
};

//-------------------------------------------------
//--------- Mover elementos entre listas ----------

interface moveItemsBetweenListsProps {
  from: any[];
  to: any[];
  index: number;
}

export const moveItemsBetweenLists = ({
  from,
  to,
  index
}: moveItemsBetweenListsProps) => {
  const element = from[index];

  return [
    [...from.slice(0, index), ...from.slice(index + 1)],
    [...to.slice(0, index), element, ...to.slice(index)]
  ];
};
