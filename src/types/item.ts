import { ListIds } from "./list";

export type TItem = {
  id: string;
  parentId: keyof typeof ListIds;
  title: string;
};
