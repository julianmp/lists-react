import { TItem } from "./item";

export const ListIds = {
  list1: "list1",
  list2: "list2"
} as const;

export interface TList {
  id: keyof typeof ListIds;
  order: string[];
}

export type onMoveItemFn = (
  list: TItem[],
  index: number,
  direction: 1 | -1
) => void;
