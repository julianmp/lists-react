# GM2 Technical Interview

## General Requirements

1. Googling and any other everyday tool is accepted.
2. All questions are allowed, and even encouraged. As interviwers we can decide not to answer a question if we are evaluating on that topic, but that´s for us to decide! Ask away!
3. Code, folders and styling structure must be clean, mantainable and scalable.
4. Styling will be considered, so apply the styles you consider necessary.

## Interview Exercise Requirements

1. **Add styling to the list and list items.**
   a. List must be centered horizontally and vertically.

2. **Add four more lists.**
   a. Lists items must differ from first list.
   b. All list items must have the same behaviour as the first list (items should move inside each list).

3. **Move items between lists.**
   a. Add an action to move items to the list on the right.
